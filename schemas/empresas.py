from pydantic import BaseModel, Field
from typing import List, Optional

class Empresas(BaseModel):
    id: Optional[int]
    nombre: Optional[str]
    descripcion: Optional[str]
    email: Optional[str]
    telefono: Optional[str]
    firma: Optional[str]
    web: Optional[str]

    model_config = {
        "json_schema_extra": {
            "examples": [
                {
                    "id": 1,
                    "nombre": "MiPelicula",
                    "descripcion": "MiDescripcion",
                    "email": 2022,
                    "telefono": 9.8,
                    "firma": "action",
                    "web": "",
                }
            ]
        }
    }