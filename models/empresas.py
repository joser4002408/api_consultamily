from sqlalchemy import Column, Integer, String, Float
from config.database import Base

class Empresas(Base):
    __tablename__ = "empresas"
    id = Column(Integer, primary_key=True)
    nombre = Column(String)
    descripcion = Column(String)
    email = Column(String)
    telefono = Column(String)
    firma = Column(String)
    web = Column(String)
    direccion = Column(String)
