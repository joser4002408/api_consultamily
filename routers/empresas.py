from fastapi import APIRouter, status, Body, Depends
from fastapi.responses import JSONResponse

from config.database import mycursor, engine, SessionLocal

from services.empresas import EmpresasService as EmpresasService
from models.empresas import Empresas as EmpresasModels
from schemas.empresas import Empresas as Empresas

from typing import List, Optional

from fastapi.encoders import jsonable_encoder

from pydantic import BaseModel


empresas_router = APIRouter()

# AGREGAMOS EL MODELO DE PYDANTIC
class EmpresasModelsP(BaseModel):
    nombre: str | None = None
    descripcion: str | None = None
    email: str | None = None
    telefono: str | None = None
    firma: str | None = None
    web: str | None = None

class EmpresasModelsBultos(BaseModel):
    Firma: str
    Web: Optional[str] = None  # Optional field for nullable values
    Email: Optional[str] = None
    Tel: Optional[str] = None
    Location: Optional[str] = None

@empresas_router.get("/empresas", tags=["Empresas"], response_model=List[Empresas])
async def get_empresas() -> List[Empresas]:
    """
    Obtiene todas las empresas de la base de datos.

    Devuelve:
        Una lista de tuplas con los datos de las empresas.
    """

    
    db = SessionLocal()
    
    empresas = EmpresasService(db).get_empresas()

    return JSONResponse(content=jsonable_encoder(empresas), status_code=status.HTTP_200_OK)

# ACTUALIZAMOS LA EMPRESA
@empresas_router.put("/empresas/{id}", tags=["Empresas"], response_model=Empresas)
def update_empresa(id: int, empresa: Empresas):
    """
    Actualiza un proyecto de la base de datos.

    Parámetros:
        id (int): El id del proyecto a actualizar.
        proyecto (Proyecto): Los datos del proyecto a actualizar.

    Devuelve:
        Una tupla con los datos del proyecto actualizados.
    """

    db = SessionLocal()
    empresa = EmpresasService(db).update_empresas(id, empresa)

    return JSONResponse(content=jsonable_encoder(empresa), status_code=status.HTTP_200_OK)
# TRAEMOS UNA EN ESPECIFICO
@empresas_router.get("/empresas/{id}", tags=["Empresas"], response_model=Empresas)
def get_empresa(id: int):
    """
    Obtiene un proyecto de la base de datos.

    Parámetros:
        id (int): El id del proyecto a obtener.

    Devuelve:
        Una tupla con los datos del proyecto.
    """

    db = SessionLocal()
    empresa = EmpresasService(db).get_empresas_id(id)

    return JSONResponse(content=jsonable_encoder(empresa), status_code=status.HTTP_200_OK)

# CONSULTA DE EMPRESAS POR NOMBRE
@empresas_router.get("/empresas/", tags=["Empresas"], response_model=List[EmpresasModelsP])
def get_empresas_body(model: EmpresasModelsP = Body(...)):
    if model.nombre:
        db = SessionLocal()
        empresas = EmpresasService(db).get_empresas_nombre(model.nombre)
        return JSONResponse(content=jsonable_encoder(empresas), status_code=status.HTTP_200_OK)
        return {"Hola"}
    elif model.firma:
        db = SessionLocal()
        empresas = EmpresasService(db).get_empresas_nombre(model.firma)
        return JSONResponse(content=jsonable_encoder(empresas), status_code=status.HTTP_200_OK)
    else:
        return JSONResponse(content=jsonable_encoder({"error": "No se encontraron resultados"}), status_code=status.HTTP_404_NOT_FOUND)

# CONSULTA EN BULTOS
@empresas_router.post("/empresas/check", tags=["Empresas"])
async def check_empresas(empresas: List[EmpresasModelsBultos] = Body(...)):
    # ... (your logic to check against the database)
    db = SessionLocal()

    devolveremos = []

    print("Aqui llega!")

    for empresa in empresas:
        # print(empresa.Firma)
        # print("^^^^")
        empresas_db = EmpresasService(db).get_empresas_nombre(empresa.Firma)
        if empresas_db:
            variables = jsonable_encoder(empresas_db)
            # print(variables)
            print(f"Empresa {empresa.Firma} Existe en la base de datos")
            # print(variables['firma'])
            # print(f"Empresa {variables} Existe en la base de datos")
            devolveremos.append(
                {
                    "Firma": variables['firma'], 
                    "Web": variables['web'],
                    "Email": variables['email'],
                    "Tel": variables['telefono'],
                    "Location": variables['direccion']
                })
            
            # MANDAMOS A ACTUALZIAR
            la_empresas = EmpresasModels(
                nombre=empresa.Firma,
                descripcion="",
                email=empresa.Email,
                telefono=empresa.Tel,
                firma=empresa.Firma,
                web=empresa.Web
            )

            # print(empresa)

            # print(la_empresas)

            EmpresasService(db).update_empresas(variables['id'], la_empresas)

        else:
            print(f"Empresa {empresa.Firma} no existe en la base de datos")

            # MANDAMOS A CREAR
            la_empresas = EmpresasModels(
                nombre=empresa.Firma,
                descripcion="",
                email=empresa.Email,
                telefono=empresa.Tel,
                firma=empresa.Firma,
                web=empresa.Web
            )

            # print(la_empresas)

            EmpresasService(db).create_empresas(la_empresas)

    return JSONResponse(content=jsonable_encoder(devolveremos), status_code=status.HTTP_200_OK)

# ACTUALIZA EMPRESAS EN BULTOS
@empresas_router.put("/empresas/update", tags=["Empresas"])
async def update_empresas(empresas: List[EmpresasModelsBultos] = Body(...)):
    # ... (your logic to check against the database)
    db = SessionLocal()

    devolveremos = []

    for empresa in empresas:
        empresas_db = EmpresasService(db).get_empresas_nombre(empresa.Firma)
        if empresas_db:
            variables = jsonable_encoder(empresas_db)
            # print(variables)
            print(f"Empresa {empresa.Firma} Existe en la base de datos")
            # print(variables['firma'])
            # print(f"Empresa {variables} Existe en la base de datos")
            devolveremos.append(
                {
                    "Firma": variables['firma'], 
                    "Web": variables['web'],
                    "Email": variables['email'],
                    "Tel": variables['telefono'],
                    "Location": variables['direccion']
                })
        else:
            print(f"Empresa {empresa.Firma} no existe en la base de datos")

    return JSONResponse(content=jsonable_encoder(devolveremos), status_code=status.HTTP_200_OK)