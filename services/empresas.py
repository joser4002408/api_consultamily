
from models.empresas import Empresas as EmpresasModels
from schemas.empresas import Empresas as Empresas

class EmpresasService:
    def __init__(self, db) -> None:
        self.db = db

    def get_empresas(self):
        """
        Obtiene todos los empresas de la base de datos.

        Devuelve:
            Una lista de objetos EmpresasModels.
        """

        # Usa la consulta con tu clase modelo
        resultado = self.db.query(EmpresasModels).limit(100).all()
        # print(resultado)
        return resultado

    
    def get_empresas_id(self, id):
        """
        Obtiene un proyecto de la base de datos.

        Parámetros:
            id (int): El id del proyecto a obtener.

        Devuelve:
            Una tupla con los datos del proyecto.
        """

        resultado = self.db.query(EmpresasModels).filter(EmpresasModels.id == id).first()
        return resultado
    def create_empresas(self, empresas: Empresas):
        """
        Crea un proyecto en la base de datos.

        Parámetros:
            empresas (Empresas): Los datos del proyecto a crear.

        Devuelve:
            El proyecto creado.
        """

        # Crea una instancia de tu clase modelo
        empresa = EmpresasModels(
            nombre=empresas.nombre,
            descripcion=empresas.descripcion,
            email=empresas.email,
            telefono=empresas.telefono,
            firma=empresas.firma,
            web=empresas.web
        )

        # Agrega el proyecto a la sesión
        self.db.add(empresa)
        self.db.commit()
        self.db.refresh(empresa)

        return empresa
    def update_empresas(self, id: int, empresas: Empresas):
        """
        Actualiza un proyecto en la base de datos.

        Parámetros:
            id (int): El id del proyecto a actualizar.
            empresas (Empresas): Los datos del proyecto a actualizar.

        Devuelve:
            El proyecto actualizado.
        """

        # Usa la consulta con tu clase modelo
        empresa = self.db.query(EmpresasModels).filter(EmpresasModels.id == id).first()
        print("Services dice:")
        print(id)
        print(empresa)
        print("fin <--")

        # Actualiza el proyecto
        empresa.nombre = empresas.nombre
        empresa.descripcion = empresas.descripcion
        empresa.email = empresas.email
        empresa.telefono = empresas.telefono
        empresa.firma = empresas.firma
        empresa.web = empresas.web

        self.db.commit()
        self.db.refresh(empresa)

        return empresa
    # def delete_empresas(self, id: int):
    #     """
    #     Elimina un proyecto de la base de datos.
    def get_empresas_nombre(self, nombre):
        """
        Obtiene un proyecto de la base de datos.

        Parámetros:
            id (int): El id del proyecto a obtener.

        Devuelve:
            Una tupla con los datos del proyecto.
        """

        resultado = self.db.query(EmpresasModels).filter(EmpresasModels.nombre == nombre).first()
        return resultado