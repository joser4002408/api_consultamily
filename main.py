from fastapi import FastAPI, status
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel

from routers.empresas import empresas_router

from middlewares.error_handler import ErrorHandler

# TODO AGREGADO PARA PODER CONECTAR EN LOCALHOST
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(ErrorHandler)
app.include_router(empresas_router)

# TODO AGREGADO PARA PODER CONECTAR EN LOCALHOST
# Configuración de CORS
origins = [
    "http://127.0.0.1:8000",
]
# TODO AGREGADO PARA PODER CONECTAR EN LOCALHOST
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

@app.get("/", response_class=HTMLResponse, tags=["Home"])
async def read_root():
    return HTMLResponse(content="<h1>Welcome to Consultamily</h1>")
